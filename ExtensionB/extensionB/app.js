require.config({
    baseUrl: 'Scripts/',
    paths: {
        jquery: 'jquery-2.1.4.min',
        signalR: 'jquery.signalR-2.2.0.min',
        qPromise: 'q',
        signalRService: '../js/service/signalrservice'
    },
    shim: {
        jquery: {
            exports: '$'
        },
        qPromise: {
            exports: 'Q'
        },
        signalR: {
            exports: 'signalR',
            deps: ['jquery']
        },
        signalRService: {
            exports: 'signalRService',
            deps: ['jquery', 'signalR', 'qPromise']
        }
    }
});
require(['jquery', 'signalRService', 'qPromise'], function ($, sr, Q) {
    $("#btn1").on('click', function () {
        //When btn1 clicked send data to parent window
        var router = new sr.SignalRService.router(Q);
        router.callServer(document.getElementById("text2").value);
    });
    window.addEventListener("message", function (evt) {
        var message;
        var txt = document.getElementById("text1");
        txt.value = evt.data;
    }, false);
});
//# sourceMappingURL=app.js.map