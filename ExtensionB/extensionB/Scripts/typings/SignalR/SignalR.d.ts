﻿interface connection {
    hub: any;
}
interface signalR {
    hubConnection(url: string, option?: any): any
    connection: connection;
}

interface JQueryStatic extends signalR {

}