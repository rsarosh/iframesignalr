/// <reference path="../../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../../scripts/typings/signalr/signalr.d.ts" />
/// <reference path="../../scripts/typings/q/q.d.ts" />
define(["require", "exports"], function (require, exports) {
    var SignalRService;
    (function (SignalRService) {
        var router = (function () {
            function router(qPromise) {
                this._q = qPromise;
                this.connection = $.hubConnection("http://localhost:8082");
                this.hubProxy = this.connection.createHubProxy('serverHub');
            }
            router.prototype.callServer = function (para) {
                var _this = this;
                var deferred = this._q.defer();
                this.connection.start()
                    .done(function () {
                    _this.hubProxy.invoke("serverMethod", para).done(function (resp) {
                        deferred.resolve(resp);
                    });
                })
                    .fail(function () {
                    console.log('Could not Connect!');
                    deferred.reject("error");
                });
                return deferred.promise;
            };
            ;
            return router;
        })();
        SignalRService.router = router;
        ;
    })(SignalRService = exports.SignalRService || (exports.SignalRService = {}));
    ;
});
//# sourceMappingURL=signalrservice.js.map