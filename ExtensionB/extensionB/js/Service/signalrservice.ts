/// <reference path="../../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../../scripts/typings/signalr/signalr.d.ts" />
/// <reference path="../../scripts/typings/q/q.d.ts" />

export module SignalRService {

    export class router<T> {
        private connection: any;
        private hubProxy: any;
        private _q: Q<any>;

        public constructor(qPromise: Q<any>) {
            this._q = qPromise;
            this.connection = $.hubConnection("http://localhost:8082");
            this.hubProxy = this.connection.createHubProxy('serverHub');
        }

        public callServer(para: string): Q.Promise<T> {
            let deferred = this._q.defer();
            this.connection.start()
                .done(() => {
                        this.hubProxy.invoke("serverMethod", para).done((resp) => {
                        deferred.resolve(resp);
                    });
                })
                .fail(function () {
                    console.log('Could not Connect!');
                    deferred.reject("error");
                });
            return deferred.promise;
        };
    };
};
