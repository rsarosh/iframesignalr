﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace SignalRServer
{
    public class ServerHub: Hub 
    {
         static IHubContext _context = null;
        
        /// <summary>
        /// Context instance to access client connections to broadcast to
        /// </summary>
        public static IHubContext HubContext
        {
            get
            {
                if (_context == null)
                    _context = GlobalHost.ConnectionManager.GetHubContext<ServerHub>();

                return _context;
            }
        }


        /// <summary>
        /// This method calls client method directly. Clients must have a "clientMethod" method define
        /// </summary>
        /// <param name="message"></param>
        public static void WriteMessage(string message)
        {
            try
            {
                // Write out message to SignalR clients  
                HubContext.Clients.All.clientMethod(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        
        /// <summary>
        /// Clients will call this server message
        /// </summary>
        /// <param name="someString"></param>
        /// 
        public void serverMethod (string someString){
            Console.Write("*");
            WriteMessage(someString);
        }
        
    };

}
