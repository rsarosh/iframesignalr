﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SignalRServer
{
    class Program
    {
        static void Main(string[] args)
        {
            IDisposable SignalR = WebApp.Start<OwinStartup>("http://localhost:8082/");

            while (true)
            {
                Console.Write("."); //Show heartbeat on console
                Thread.Sleep(500);
            }

        }
    }
}
