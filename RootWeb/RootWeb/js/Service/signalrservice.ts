/// <reference path="../../packages/signalr/signalr.d.ts" />
/// <reference path="../../packages/q.typescript.definitelytyped.0.7.6/content/scripts/typings/q/q.d.ts" />
/// <reference path="../../packages/jquery.typescript.definitelytyped.2.3.3/content/scripts/typings/jquery/jquery.d.ts" />

export module SignalRService {

    export class router {
        private connection: any;
        private hubProxy: any;

        public constructor() {
            this.connection = $.hubConnection("http://localhost:8082");
            this.hubProxy = this.connection.createHubProxy('serverHub');

            //This method "ClientMethod" will ve called by SignalR server.
            this.hubProxy.on('clientMethod', function (message: string) {
                (<any>document.getElementById("text1")).value = message;
            });

            //Have connection open and ready to listen 
            this.connection.start()
                .done(() => {
                    console.log('Now connected');
                })
                .fail(function () {
                    console.log('Could not Connect!');
                });
         
        }

        /*
           These methods are not used. They can be used if rootweb wants to send
           data to SignalR. But in this case it is only receiving messages from SignalR
           server 

        private InvokeMethod (methodName) {
            var deferred = this.$q.defer();
            this.connection.start()
                .done(() => {
                    console.log('Now connected');
                    this.hubProxy.invoke(methodName).done((resp) =>{
                        deferred.resolve(resp);
                    });
                })
                .fail(function () {
                    console.log('Could not Connect!');
                    deferred.reject("error");
                });
            return deferred.promise;
        };

 
        public broadcast(broadcastString): any {
            var defer = this.$q.defer();
            var self = this;
            this.InvokeMethod(broadcastString).then(function (data) {
                var parsed = JSON.parse(data);
                defer.resolve(parsed);
            }).catch(function (reason) {
                defer.reject(reason);
            });
            return { promise: defer.promise };
        };
        */
    };
};
