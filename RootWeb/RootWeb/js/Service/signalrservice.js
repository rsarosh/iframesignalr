/// <reference path="../../packages/signalr/signalr.d.ts" />
/// <reference path="../../packages/q.typescript.definitelytyped.0.7.6/content/scripts/typings/q/q.d.ts" />
/// <reference path="../../packages/jquery.typescript.definitelytyped.2.3.3/content/scripts/typings/jquery/jquery.d.ts" />
define(["require", "exports"], function (require, exports) {
    var SignalRService;
    (function (SignalRService) {
        var router = (function () {
            function router() {
                this.connection = $.hubConnection("http://localhost:8082");
                this.hubProxy = this.connection.createHubProxy('serverHub');
                //This method "ClientMethod" will ve called by SignalR server.
                this.hubProxy.on('clientMethod', function (message) {
                    document.getElementById("text1").value = message;
                });
                //Have connection open and ready to listen 
                this.connection.start()
                    .done(function () {
                    console.log('Now connected');
                })
                    .fail(function () {
                    console.log('Could not Connect!');
                });
            }
            return router;
        })();
        SignalRService.router = router;
        ;
    })(SignalRService = exports.SignalRService || (exports.SignalRService = {}));
    ;
});
//# sourceMappingURL=signalrservice.js.map