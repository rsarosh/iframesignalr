/// <reference path="js/service/signalrservice.ts" />
/// <reference path="packages/requirejs.2.1.6/typed/require.d.ts" />
require.config({
    baseUrl: 'packages/',
    paths: {
        jquery: 'jQuery.2.1.4/content/Scripts/jquery-2.1.4.min',
        signalR: 'Microsoft.AspNet.SignalR.JS.2.2.0/content/Scripts/jquery.signalR-2.2.0.min',
        qPromise: 'Q.1.4.1/content/Scripts/q',
        signalRService: '../js/service/signalrservice',
        app: '../app'
    },
    shim: {
        jquery: {
            exports: '$'
        },
        qPromise: {
            exports: 'Q'
        },
        signalR: {
            exports: 'signalR',
            deps: ['jquery']
        },
        signalRService: {
            exports: 'signalRService',
            deps: ['jquery', 'signalR', 'qPromise']
        },
        app: {
            exports: 'app'
        }
    }
});
//# sourceMappingURL=initApp.js.map