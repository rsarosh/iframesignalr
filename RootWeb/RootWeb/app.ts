﻿export module app {
    define('server', ['jquery', 'signalRService', 'qPromise'], ($, sr, Q) => {

        function initRouter() {
            var router = new sr.SignalRService.router();
        };

        function makeFrame() {
            var ifrm = document.createElement("IFRAME");
            ifrm.id = "iframe";
            ifrm.setAttribute("src", "http://localhost:16852/index.html");
            ifrm.style.width = 640 + "px";
            ifrm.style.height = 480 + "px";
            var content: any = document.getElementById("content");
            content.appendChild(ifrm);
            var iframe: any = document.getElementById("iframe");
            var iframeWindow = iframe.contentWindow
            iframeWindow.addEventListener("message", receiveData, false);

            //initRouter
            initRouter();
        } 

        function receiveData(evt: any) {
            var message;
            var txt: any = document.getElementById("text1");
            txt.value = evt.data;
            
        }
        function sendDataToIFrame() {

            var iframe: any = document.getElementById("iframe");
            iframe = iframe.contentWindow;
            var textVal: any = document.getElementById('text1'); 
            iframe.postMessage(textVal.value, "http://localhost:16852/index.html");
        } 

        return {
            initRouter: initRouter,
            makeFrame: makeFrame,
            sendDataToIFrame: sendDataToIFrame
        };

    });

}

export {app as default};
